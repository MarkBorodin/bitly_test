from django.contrib import admin
from django.urls import include, path

from app.views import Index, AccountProfile

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django_registration.backends.one_step.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/profile/', AccountProfile.as_view(), name='account_profile'),
    path('', Index.as_view(), name='index'),
    path('url/', include('bitly.urls')),
]
