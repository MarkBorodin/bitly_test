import datetime

from django.contrib.auth.models import User
from django.db import models


class BaseModel(models.Model):
    """
    Base abstract model for all models
    """
    class Meta:
        abstract = True
    create_date = models.DateField(null=True, default=datetime.date.today, editable=True)
    write_date = models.DateField(null=True, auto_now=True, editable=True)


class Url(BaseModel):
    base_url = models.URLField(max_length=2048, null=False, blank=False)
    short_url = models.URLField(max_length=128)
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    counter = models.SmallIntegerField(null=True, blank=True, default=0)
