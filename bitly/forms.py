from django import forms

from bitly.models import Url


class UrlForm(forms.ModelForm):
    class Meta:
        model = Url
        fields = ['base_url']
        labels = {
            "base_url": "Enter your url"
        }
