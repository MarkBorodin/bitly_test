import secrets

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpRequest
from django.shortcuts import render

from bitly.forms import UrlForm
from bitly.models import Url
from bitly.utils.constants import chars


@login_required
def get_or_create_url(request: HttpRequest) -> render:
    """
    Get or create an 'Url' object and create a shortened url
    :param request: HttpRequest
    :return: render
    """
    if request.method == 'POST':
        form = UrlForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            obj, created = Url.objects.get_or_create(
                base_url=cleaned_data['base_url'],
                user=request.user,
                defaults={'short_url': "".join(secrets.choice(chars) for _ in range(5))},
            )
            return render(request, 'bitly/result.html', {'obj': obj, 'created': created})
    else:
        form = UrlForm()
        return render(request, 'bitly/create_url.html', {'form': form})


@login_required
def redirect_by_url(request: HttpRequest, pk: int) -> HttpResponseRedirect:
    """
    Redirect from shortened url to real url
    :param request: HttpRequest
    :param pk: int
    :return: HttpResponseRedirect
    """
    obj = Url.objects.get(pk=pk)
    obj.counter += 1
    obj.save()
    return HttpResponseRedirect(obj.base_url)
