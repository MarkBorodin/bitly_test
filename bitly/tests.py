from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from bitly.models import Url


class UrlTestCase(TestCase):
    def setUp(self):
        # user1
        self.credentials1 = {
            'username': 'testuser1',
            'email': 'email1@mail.com',
            'password': 'secret'}
        User.objects.create_user(**self.credentials1)

        # user2
        self.credentials2 = {
            'username': 'testuser2',
            'email': 'email2@mail.com',
            'password': 'secret'}
        User.objects.create_user(**self.credentials2)

        # other data
        self.base_url_youtube = 'https://www.youtube.com/'
        self.form_data = urlencode({'base_url': self.base_url_youtube})

    def test_url_create_object(self):
        # create shot_url form user1
        self.client.login(username='testuser1', password='secret')
        response = self.client.post(
            reverse('bitly:create_url'),
            self.form_data,
            follow=True,
            content_type="application/x-www-form-urlencoded"
        )
        user1 = response.wsgi_request.user
        obj1 = Url.objects.get(base_url=self.base_url_youtube, user=user1)
        self.assertTrue(obj1, True)
        self.assertTrue(obj1.short_url, True)
        self.assertEqual(obj1.counter, 0)

        # follow link user1
        self.client.get(
            reverse('bitly:redirect_by_url', kwargs={'pk': obj1.pk}),
            follow=True,
            content_type="application/x-www-form-urlencoded"
        )
        updated_obj1 = Url.objects.get(base_url=self.base_url_youtube, user=user1)
        self.assertEqual(updated_obj1.counter, 1)

        # create shot_url form user2
        self.client.login(username='testuser2', password='secret')
        response = self.client.post(
            reverse('bitly:create_url'),
            self.form_data,
            follow=True,
            content_type="application/x-www-form-urlencoded"
        )
        user2 = response.wsgi_request.user
        obj2 = Url.objects.get(base_url=self.base_url_youtube, user=user2)
        self.assertTrue(obj2, True)
        self.assertTrue(obj2.short_url, True)
        self.assertEqual(obj2.counter, 0)
        self.assertNotEqual(updated_obj1.counter, obj2.counter)
