from django.urls import path

from bitly.views import get_or_create_url, redirect_by_url

app_name = "bitly"

urlpatterns = [
    path('create', get_or_create_url, name='create_url'),
    path('<int:pk>', redirect_by_url, name='redirect_by_url'),
    ]
